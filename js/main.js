/* 
   1. Функції потрібні для того, щоб можна було використовувати функціонал який вона виконує, багато разів та будь де не повторюючи код

   2. Аргументи це значення, які передаються функції під час її виклику, тобто функція буде виконувати дії, а значення будемо 
      передавати через аргументи в параметри функції і ці аргументи це змінні які використовуються в середені функції самої фуекції.

   3. Оператор return повертає результат функції, його потрібно ставити вкінці функції, також закінчує роботу функції,
      якщо щось на другому рядку після return - нічого не буде виконуватися і якщо у функції немає return, вона повертає undefined 
 */


let firstNumber = +prompt("Введіть будь-ласка перше число");
let secondNumber = +prompt("Введіть будь-ласка друге число");

while ((!firstNumber && !secondNumber) || (isNaN(firstNumber) && isNaN(secondNumber))) {
   firstNumber = +prompt("Введіть будь-ласка перше число", firstNumber);
   secondNumber = +prompt("Введіть будь-ласка друге число", secondNumber);
}

let mathOperator = prompt("Введіть будь-ласка один математичний знак +, -, *, /");

while (mathOperator !== "+" && mathOperator !== "-" && mathOperator !== "*" && mathOperator !== "/") {
   mathOperator = prompt("Введіть будь-ласка один математичний знак +, -, *, /");
}

let result;

function getValue (arg1, arg2, operator) {
   switch (operator) {
      case "+":
         result = arg1 + arg2;
         break;
      case "-":
         result = arg1 - arg2;
         break;
      case "*":
         result = arg1 * arg2;
         break;
      case "/":
         result = arg1 / arg2;
         break;
   }
   return result;
}

const functionResult = getValue(firstNumber, secondNumber, mathOperator);
console.log(functionResult);















